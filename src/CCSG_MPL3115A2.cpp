/*******************************************************************************
 * CCSG MAX17262 Driver
 *
 * Copyright (c) 2020 by Bob Iannucci
 *******************************************************************************/

#include "CCSG_MPL3115A2.h"
#include "CCSG_Debug.h"
#include <Arduino.h>
#include <Wire.h>



CCSG_MPL3115A2::CCSG_MPL3115A2()
{
}


boolean CCSG_MPL3115A2::begin(TwoWire *twoWire)
{
	// uint8_t whoAmI;
	i2cBus = twoWire;
	i2cBus->begin();

	return (read8((uint8_t)MPL3115A2_WHOAMI) == 0xC4);
}


void CCSG_MPL3115A2::dumpRegisters(uint8_t start, uint8_t end)
{
	uint8_t regAddr;
	uint8_t response;

	for (regAddr=start; regAddr<end; regAddr++)
	{
		response = read8(regAddr);
		CCSG_DEBUG_PRINTF("[baro    ] Register 0x%02X has value 0x%02X\r\n", regAddr, response);
	}
}


uint32_t CCSG_MPL3115A2::getPressure()
{
	uint8_t msb;
	uint8_t csb;
	uint8_t lsb;
	uint32_t pressure;

	// per Fig 7 in the datasheet
	write8(MPL3115A2_CTRL_REG1, MPL3115A2_CTRL_REG1_OS128);				// Set to BAROMETER mode with an OSR = 128
	write8(MPL3115A2_PT_DATA_CFG, MPL3115A2_PT_DATA_CFG_PDEFE);			// Enable only the PDEFE (pressure data ready) flag

	write8(MPL3115A2_CTRL_REG1, MPL3115A2_CTRL_REG1_OS128 | 0x02);		// Set active (one-shot): perform a measurement, then return to STANDBY state

	// Note that reading MPL3115A2_REGISTER_STATUS will yield the non-FIFO status, since the default F_MODE is 00
	// So this is the same as reading register 0x06
	while((read8(MPL3115A2_REGISTER_STATUS) & MPL3115A2_REGISTER_STATUS_PDR) == 0)
	{
		delay(10);	// spin, waiting for PDR:  Pressure/altitude or temperature data ready     **FIXME** infinite loop
	}

	msb = read8((uint8_t)MPL3115A2_REGISTER_PRESSURE_MSB);  // i2cBus->read();
	csb = read8((uint8_t)MPL3115A2_REGISTER_PRESSURE_CSB);  // i2cBus->read();
	lsb = read8((uint8_t)MPL3115A2_REGISTER_PRESSURE_LSB);  // i2cBus->read();
	pressure = ((msb << 16) | (csb << 8) | lsb) >> 6;

	return pressure;
}


// NOTE: Arduino Due and the associated Wire and TWI libraries use a SPECIAL form of the I2C read operation
// that implements a RESTART rather than a WRITE-STOP-READ (which confuses some devices like this one).
//
// Applicable to Arduino SAM implementation of Wire.cpp and twi.c
//
// See https://forum.arduino.cc/index.php?topic=385377.0
//
uint8_t CCSG_MPL3115A2::read8(uint8_t regAddress)
{
	uint8_t dataRead;

	// DO NOT use this standard I2C / Arduino idiom:
//	i2cBus->beginTransmission(MPL3115A2_ADDRESS);
//	i2cBus->write(regAddress);										// REMEMBER: write() only deposits something in the outbound buffer
//	i2cBus->endTransmission(false);									// endTransmission() is what delays us until the buffer empties
//	i2cBus->requestFrom((uint8_t)MPL3115A2_ADDRESS, (uint8_t) 1);	// performs a BLOCKING READ into the buffer
//	dataRead = i2cBus->read();										// extracts from the buffer
//	return dataRead;

	// Instead, use this idiom that implements restart instead of stop-start:
	i2cBus->requestFrom((uint8_t)MPL3115A2_ADDRESS, (uint8_t) 1, (uint8_t)regAddress, (uint8_t) 1, (uint8_t) true);
	dataRead = i2cBus->read();										// extracts from the buffer
	return dataRead;
}


void CCSG_MPL3115A2::write8(uint8_t regAddress, uint8_t data)
{
	i2cBus->beginTransmission(MPL3115A2_ADDRESS);
	i2cBus->write(regAddress);
	i2cBus->write(data);
	i2cBus->endTransmission(false);
}
