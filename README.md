# CCSG Barometer Library

This library offers functions for configuring and reading the MPL3115A2 barometric pressure sensor used by the CCSG EnviSense family of boards.